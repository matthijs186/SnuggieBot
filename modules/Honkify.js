const Module = require('../classes/Module.js').IModule;

class Honkify extends Module
{
    constructor()
    {
        super("Honkify", "honk");
    }

    OnBotStart(bot)
    {
        this.honkCount = 0;
    }
    OnMessageReceived(bot, message)
    {
        if(message.content === bot.settings.bot_command_character + this.command)
        {
            ///TODO: Add rewards
            ///TODO: Reset the honk count each day
            ///TODO: Add a total honk counts
            message.reply("Today bane honked " + this.honkCount + " times");
        }
        else if((message.content.toLowerCase().indexOf("honk") != -1) && 
             (message.author.id === '176147051178885120'))
        {

            //Decides if answer to the HONK or not
            let chance = Math.floor(Math.random() * 100);
            if(chance > 98)
            {
                message.channel.send('*HONKS EVEN STRONGER*').then(() => {console.log("I honkd")}).catch(console.error);
            }
            this.honkCount ++;
        }
    }

    OnBotSaveData(bot)
    {
        ///TODO: Implement save logic
    }
}

exports.Module = Honkify;