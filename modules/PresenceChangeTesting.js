const IModule = require('../classes/Module.js').IModule;

class PresenceChange extends IModule
{

    constructor()
    {
        super("PresenceChange","presence")
        this.present = true;
    }

    OnMessageReceived(bot, message)
    {
        //Bot is set to answer only to a specifi user dying
        ///TODO: Use a regex to see if the message was sent in an RP context
        ///TODO: add more replies
        if(message.content === bot.settings.bot_command_character + this.command)
        {
            message.reply(this.present === true ? "is online" : "is offline")
            .then(() => {console.log("Presence test")})
            .catch(console.error);
        }
    }

    OnPresenceChange(oldPresence, newPresence)
    {
        console.log("Managing presence update");
        if(newPresence.id == '87108085516079104')
        {
            if(newPresence.presence.status === 'online')
                this.present = true;
            else   
                this.present = false;
        }
    }

}

exports.Module = PresenceChange;