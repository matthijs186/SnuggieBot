const IModule = require('../classes/Module.js').IModule;
const Util = require('util');
const Utils = require('../classes/Utils.js').Utils;

class SnugglyBot extends IModule
{

    constructor()
    {
        super("SnugglyBot","")
    }

    OnBotReadData(bot)
    {
        this.Settings = bot.settings.misc.snuggly_settings;
        this.Phrases = this.Settings.snuggle_phrases;

        if(typeof this.Phrases === 'undefined' || this.Phrases.length == 0)
            {
                this.Phrases =[
                    "*Snuggles %s ^^*",
                    "*Jumps on %s and snuggles him*",
                    "Awww... *snugs %s tightly*",
                    "eee... who's a cute pone?\neee Who's a cute pone? Chu are! \n*snugs %s tightly*",
                    "Behold! For i am the Goddess of Snugs! *snugs %s*"
                ]
            }

        this.log("Initializing SnugglyBot Module")
        this.log("Phrases: \n " + this.Phrases);

        this.log("Settings loaded correctly!");
    }

    OnMessageReceived(bot, message)
    {

        //TODO: Improve algorithm
        if((message.content.contains("snugs")&& message.content.contains("require")) ||
            (message.content.contains("snug")&& message.content.contains("me")) ||
            (message.content.contains("snugs") && message.content.contains("want")))
        {
            let index = Math.floor(Math.random() * this.Phrases.length);
            let phrase = Util.format(this.Phrases[index], Utils.GetMentionString(message.author.id));
            message.channel.send(phrase)
                .then(() => {this.log("Cute message sent: " + phrase)})
                .catch(console.error);
        }
        ///TODO: Need to Regex this
        else if(message.content.contains("snug") && 
                message.content.length >= 2)
        {
            if(Utils.IsAMention(message.content.split(' ')[1]))
            {
            let snugTarget = message.content.split(' ')[1];
            if(Utils.RemoveMention(snugTarget) == bot.user.id)
            {
                message.channel.send("*Snugs herself*")
                .then(()=>{this.log("User wanted me to snug myself >c<")})
                .catch(console.error);
            }
            else
            {
                let index = Math.floor(Math.random() * this.Phrases.length);
                let phrase = Util.format(this.Phrases[index], snugTarget);
                message.channel.send(phrase)
                    .then(() => {this.log("Cute message sent: " + phrase)})
                    .catch(console.error);
            }
        }
        }
    }
}

exports.Module = SnugglyBot