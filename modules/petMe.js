const Util = require('util');
const Module = require('../classes/Module.js').IModule;
const Utils = require('../classes/Utils.js').Utils;

class PetModule extends Module
{
    constructor()
    {
        super("PetModule","pet");
    }
    OnMessageReceived(bot, message)
    {
        if(message.content === bot.settings.bot_command_character + 'pet')
        {
            ///TODO: De-boilerplaterize this code
            //As requested, i'm checking if the message was sent by a certain user,
            //in which case the bot is going to reply with specific messagges
            if(message.author.id === '186310820794204160') 
            {
                let replies = 
                [
                    "That's a good Middy~\n*Scritches underneath her chin*."
                ]
    
                let index = Math.floor(Math.random() * replies.length);
    
                let cuteString = replies[index];
                message.channel.send(cuteString)
                                .then((msg) => {console.log('Sent special message: ' + cuteString)
                                .catch(console.error)})
            }
            else
            {
                ///TODO: Move the replies to (settings.json).misc.pet.phrases
                let replies = 
                [
                    "*Pets %s*",
                    "Mrrr... \n*purrs softly and pets %s*",
                    "*Snugs %s*",
                    "*Jumps on %s and snugs him*"
                ]
    
                let index = Math.floor(Math.random() * replies.length);
    
                let cuteString = Util.format(replies[index], Utils.GetMentionString(message.author.id));
                message.channel.send(cuteString)
                                .then((msg) => {console.log('Sent message: ' + cuteString)
                                .catch(console.error)})
            }
           
        }
    }
}

exports.Module = PetModule;