const Path = require('path');

const Utils = require('./Utils.js').Utils;

class IModule
{
        constructor(name,commandName)
        {
                this.name = name;
                this.command = commandName;

        }      

        /**
         * Called when initializing the Bot,
         * when the configurations and such must be loaded
         * @param {Bot} bot 
         */
        OnBotReadData(bot)
        {

        }

        /**
         * Initialization function
         * @param {Bot} bot 
         */
        OnBotStart(bot)
        {
        }

        /**
         * Called when a message is received
         * @param {Bot} bot 
         * @param {Message} message 
         */
        OnMessageReceived(bot, message)
        {
        }

        OnPresenceChange(oldPresence, newPresence)
        {
                
        }
        
        /**
         * Called when the bot is shutting down
         */
        OnBotShutdown(bot)
        {
        }
        
        /**
         * Called when the bot must save the data
         * should be written as an async function
         * @param {Bot} bot 
         */
        OnBotSaveData(bot)
        {
        }

        static LoadModules(path)
        {
                let moduleArray = [];
                let files = Utils.ReadFilenamesFromDirectory(path);
                console.log("Loading modules from " + path)
                for(let i in files)
                {
                        let file = files[i];
                        if(file.endsWith(".js"))
                        {
                        const loadedModule = new (require(Path.join(path, file))).Module;
                        console.log("Loaded module %s", loadedModule.name);
                        moduleArray.push(loadedModule);
                        }
                }

                console.log("Done loading modules!")
                return moduleArray;

        }

        log(message)
        {
                console.log("[ " + this.name + " ] : " + message);
        }

}

exports.IModule = IModule;
