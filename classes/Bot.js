const Discord = require('discord.js');
const Module = require('./Module.js').IModule;
const ModuleManager = require('./ModuleManager.js').ModuleManager;

const Client = new Discord.Client();

///Going to triple-flying kick myself for the mistake i made here
let Settings = new (require('./Settings.js')).Settings('../data/Settings.json');
Client.settings = Settings;

const Manager = new ModuleManager(Module.LoadModules('../modules/'));

Manager.Start(Client);

Client.on('ready', () => {
    console.log("Bot ready");
    Manager.BotReadData(Client);
});

Client.on('presenceUpdate', (oldPresence, newPresence)=>
{
    Manager.OnPresenceChange(oldPresence, newPresence)
})

Client.on('message', (message) => {

    Manager.DispatchMessage(Client, message);

});

Client.login(Settings.bot_token);
