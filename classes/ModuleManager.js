const IModule = require('./Module.js').IModule;

class ModuleManager
{

    constructor(modulesArray)
    {
        this.IModuleArray = modulesArray;
    }

    AddModule(imodule)
    {
        if(imodule instanceof IModule)
        {
            this.IModuleArray.push(imodule);
        }
    }

    Start(bot)
    {
        this.IModuleArray.forEach(function(element) {
            element.OnBotStart(bot);
        }, this);
    }    
    
    BotReadData(bot)
    {
        this.IModuleArray.forEach((element) => {
            element.OnBotReadData(bot);
        })
    }

    Shutdown(bot)
    {
        this.IModuleArray.forEach(function(element) {
            element.OnBotShutdown(bot);
        }, this);
    }

    SaveData(bot)
    {
        this.IModuleArray.forEach(function(element) {
            element.OnBotSaveData(bot);
        }, this);
    }    
    
    LoadData(bot)
    {
        this.IModuleArray.forEach(function(element) {
            element.OnBotReadData(bot);
        }, this);
    }

    DispatchMessage(bot, message)
    {
        this.IModuleArray.forEach(function(element) {
            try
            {
                element.OnMessageReceived(bot, message);
            }
            catch (err)
            {
                console.log(err);
            }
        }, this);
    }

    OnPresenceChange(oldPresence, newPresence)
    {
        this.IModuleArray.forEach(element => {
            element.OnPresenceChange(oldPresence, newPresence)
        });
    }



}

exports.ModuleManager = ModuleManager;