const Utils = require ('./Utils.js').Utils;

class Settings {

    constructor(filePath)
        {
                if (typeof filePath !== 'undefined')
                {

                        let settingsString = Utils.ReadFileIfExists(filePath);
                        if(settingsString !== "")
                        {
                                let data = JSON.parse(settingsString);
                                //For every key in data, creates a new one into 
                                //the Settings instance and assings the read
                                //value to it
                                for(let thing in data)
                                {
                                        this[thing] = data[thing];
                                }
                        }
                        else
                        {
                                //Create a basic Settings instance which
                                //the user must edit, then closes the bot
                                this['bot_token'] = "";
                                this['bot_secret'] = "";
                                this['bot_add_token'] = "";
                                this['bot_master_admin'] = "";
                                this['bot_command_character'] = "~";
                                this['misc'] = {};
                                
                                Utils.SaveObjectToFile(filePath, this);
                                console.log("Created a new Settings file located in \"" + filePath + "\", edit it before \nrestarting the bot");
                                process.exit(0);
                        }

                }
        }

}

exports.Settings = Settings;
